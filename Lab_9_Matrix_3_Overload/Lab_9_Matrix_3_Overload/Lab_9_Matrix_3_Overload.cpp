// Lab_9_Matrix_3_Overload.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>


class SummMx
{

public:
	SummMx();
	~SummMx();
	int getElement(int rws, int clmns); // возврат элементов массива 
	bool setElement(int rws, int clmns, double el)
	{
		if (rws<rows && clmns<columns &&  rws>-1 && clmns>-1)
		{
			Mass[rws*columns + clmns] = el;
			return true;
		}
		std::cout << "Can't set element. Index Error!\n";
		return false;
	}

	void set_SummMx(int* p_Mass, int p_rows, int p_columns); // Ввод массива
	void input();
	int get_rows();
	int get_columns();// Вывод массива
	void print();
	friend std::ostream & operator<< (std::ostream &os, SummMx &rhs);
	friend SummMx operator+ (SummMx& A, SummMx& B);
	friend SummMx operator- (SummMx& A, SummMx& B);
	SummMx & operator = (SummMx B);
	
private:
	int rows;
	int columns;
	int* Mass;



};
SummMx operator + (SummMx& A, SummMx& B)
{
	std::cout << "Оператор + \n";

	if ((A.get_rows() == B.get_rows()) && (A.get_columns() == B.get_columns())) 
	{
		SummMx res;
		int* newC = new int[A.get_rows()*B.get_columns()];
		for (int i = 0; i < A.get_rows(); i++) 
		{
			for (int j = 0; j < A.get_columns(); j++) 
			{
				newC[i*A.get_columns() + j] = A.getElement(i, j) + B.getElement(i, j);
			}
		}
		res.set_SummMx(newC, A.get_rows(), B.get_columns());
		return res;
	}

}
SummMx operator -(SummMx& A, SummMx& B)
{
	std::cout << "Оператор + \n";

	if ((A.get_rows() == B.get_rows()) && (A.get_columns() == B.get_columns())) 
	{
		SummMx res;
		int* newC = new int[A.get_rows()*B.get_columns()];
		for (int i = 0; i < A.get_rows(); i++) 
		{
			for (int j = 0; j < A.get_columns(); j++) 
			{
				newC[i*A.get_columns() + j] = A.getElement(i, j) - B.getElement(i, j);
			}
		}
		res.set_SummMx(newC, A.get_rows(), B.get_columns());
		return res;
	}

}

std::ostream & operator<< (std::ostream &os, SummMx &rhs)
{

	for (int i = 0; i < rhs.get_rows(); i++)
	{
		for (int j = 0; j < rhs.get_columns(); j++)
		{

			os << rhs.getElement(i, j) << "\t";
		}
		os << std::endl;
	}
	return os;
}

SummMx::SummMx()
{
	delete[] Mass;
	int numder = 255;
	Mass = new int[numder];
	for (int i = 0; i < rows; i++) 
	{
		for (int j = 0; j < columns; j++)
		{
			Mass[i*columns + j] = 0;
		};
	};
}
SummMx::~SummMx()
{

}
void SummMx::set_SummMx(int* p_Mass, int p_rows, int p_columns)
{

	rows = p_rows;
	columns = p_columns;
	for (int i = 0; i < rows; i++) 
	{
		for (int j = 0; j < columns; j++) 
		{
			Mass[i*columns + j] = p_Mass[i*columns + j];
		};
	};

}
void SummMx::input()
{
	int new_rs;
	std::cout << "Rows = " << std::endl;
	std::cin >> new_rs;
	int new_cs;
	std::cout << "Columns = " << std::endl;
	std::cin >> new_cs;
	if (new_cs > 1 && new_cs < 10 && new_rs > 1 && new_rs < 10)
	{
		for (int i = 0; i < new_rs; i++) {
			for (int j = 0; j < new_cs; j++) 
			{
				std::cout << "input element, pls" << std::endl;
				std::cin >> Mass[i*new_cs + j];
			}
		}
	}
	rows = new_rs;
	columns = new_cs;

}
int SummMx::get_rows() {
	return rows;
}
int SummMx::get_columns()
{
	return columns;
}
int SummMx::getElement(int rws, int clmns)
{
	if (rws<rows && clmns<columns && rws>-1 && rws>-1)
	{
		return Mass[rws*columns + clmns];
	}
	std::cout << "Connot get element. Index Error!\n";
	return	-1;
}
void SummMx::print()
{
	std::cout << "Оператор Вывод \n";
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			std::cout << Mass[i*columns + j] << "\t";

		}
		std::cout << std::endl;
	}

}



SummMx & SummMx::operator=(SummMx B)
{
	delete[] Mass;
	Mass = new int[B.get_rows()*B.get_columns()];
	for (int i = 0; i < B.get_rows(); i++) 
	{
		for (int j = 0; j < B.get_columns(); j++)
		{
			Mass[i*B.get_columns() + j] = B.getElement(i, j);
		}
	}
	rows = B.get_rows();
	columns = B.get_columns();
	return *this;
}

int main()
{
	setlocale(LC_ALL, "Rus");

	SummMx matrA, matrB, matrC;
	matrA.input();
	matrA.print();
	matrB.input();
	matrB.print();
	matrC = matrA + matrB;
	matrC.print();

	return 0;

	system("pause");
	std::cout << "Hello World!\n";
}
#include "StartDialog.h"
#include "ui_mainwindow.h"

#include <QCoreApplication>
#include <QWidget>
#include <QVBoxLayout>

Qwidget w;//interface
QVBoxLayout *layout = new QVBoxLayout();
w.setLayout(layout);

Administrator *adm = new Administrator(); //model
Viewer *v = new Viewer(adm); //out
Controller *c = new Controller(adm, v); //controller

layout -> setWidget(v);
layout -> setWidget(c);
w.show();

#include "StartDialog.h"
#include <QApplication>
#include <QCoreApplication>
#include <QWidget>
#include <QVBoxLayout>

class Administrator:
        public QObject
{
    Q_OBJECT
};

int main(int argc, char *argv[])
{
    Qwidget w;//interface
    QVBoxLayout *layout = new QVBoxLayout();
    w.setLayout(layout);

    Administrator *adm = new Administrator(); //model
    Viewer *v = new Viewer(adm); //out
    Controller *c = new Controller(adm, v); //controller

    layout -> setWidget(v);
    layout -> setWidget(c);
    w.show();
    return a.exec();
}

// Lab3Massive.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>


int main()
{
	int data_type[5];
	data_type[0] = 4;
	data_type[1] = 8;
	data_type[2] = 1;
	data_type[3] = 4;
	data_type[4] = 1;
	std::cout << "int - " << data_type[0] << "\n";
	std::cout << "double - " << data_type[1] << "\n";
	std::cout << "char - " << data_type[2] << "\n";
	std::cout << "long - " << data_type[3] << "\n";
	std::cout << "bool - " << data_type[4] << "\n";
    return 0;
}


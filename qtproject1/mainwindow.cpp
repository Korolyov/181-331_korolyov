#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QLabel>
#include <QPushButton>
#include <QtGui>
#include <QVBoxLayout>
#include <QWidget>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QLabel *label = new QLabel("Summer is soon");
    QPushButton *button = new QPushButton("Exit");
    QWidget *widget = new QWidget;
    this->setCentralWidget(widget);
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(label);
    layout->addWidget(button);
    widget->setLayout(layout);
    setWindowTitle("Title");
    connect(button, SIGNAL(clicked()),this,SLOT(quit()));
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::quit()
{
    QApplication::exit();
}

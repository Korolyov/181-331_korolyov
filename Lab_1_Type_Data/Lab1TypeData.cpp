// Lab1TypeData.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>

int main()
{
	int long long a = 310;
	std::cout << "a^4=" << a*a*a*a << std::endl;
	std::cout << "a^5=" << a*a*a*a*a << std::endl;
	int c = 0x5ABC;
	std::cout << "5ABC=" << c << std::endl;
	std::cout << "\'b'+37\=" << 'b' + 37 << std::endl;
	std::cout << "\53+'b'\=" << 53 + 'b' << std::endl;
	return 0;
}


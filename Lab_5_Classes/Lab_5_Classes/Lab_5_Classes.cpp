#include "pch.h"
#include <iostream>

void swap(int *arr, int i)
{
	int p;
	p = arr[i];
	arr[i] = arr[i - 1];
	arr[i - 1] = p;
}

class sorting
{
public:
	void bubble(int *arr);
	void shaker(int *arr);
	sorting();
	const int n = 8;
private:
	int data[8];
public:
	void set_all(int *arr, int size);
	void print_array();
};

sorting::sorting() 
{
	for (int i = 0; i < 8; i++) 
	{
		data[i] = 0;
	};
}

void sorting::set_all(int *arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		data[i] = arr[i];
	}
}

void sorting::print_array()
{
	for (int i = 0; i < 8; i++)
	{
		std::cout << data[i] << " ";
	}
	std::cout << std::endl;
}

void sorting::bubble(int *arr)
{
	for (int y = 0; y < 10; y++) {
		for (int z = 0; z < 7; z++) {
			if (arr[z] > arr[z + 1]) {
				int k = arr[z];
				arr[z] = arr[z + 1];
				arr[z + 1] = k;
			}
		}
	}
	std::cout << "Bubble-Sorted: ";
	for (int i = 0; i < 8; i++)
	{
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;
}

void sorting::shaker(int *arr){
	for (int i = 0; i < 5; i++)

	{
		int n = 0;
		int m = 7;
		for (int j = n; j < m; j++)
		{
			if (arr[n] > arr[n + 1])

			{
				int t = arr[n];
				arr[n] = arr[n + 1];
				arr[n + 1] = t;
			}

			n = n + 1;

			if (arr[m - 1] > arr[m])

			{
				int t = arr[m - 1];
				arr[m - 1] = arr[m];
				arr[m] = t;
			}
			m = m - 1;
		}
	}

	std::cout << "Shaker-Sorted: ";
	for (int i = 0; i < 8; i++)
	{
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;
}

int main()
{
	std::cout << "lab5\n" << std::endl;
	int arr1[8] = { 13, 4, 45, 55, 8, 17, 9, 76 };
	int arr2[8] = { 41, 67, 4, 5, 81, 54, 0, 2 };
	std::cout << "Array_1 is bubble-sorted" << std::endl;
	sorting array1;
	array1.set_all(arr1, 8);
	array1.print_array();
	array1.bubble(arr1);
	std::cout << "Array_2 is shaker-sorted" << std::endl;
	sorting array2;
	array2.set_all(arr2, 8);
	array2.print_array();
	array2.shaker(arr2);


	return 0;
}

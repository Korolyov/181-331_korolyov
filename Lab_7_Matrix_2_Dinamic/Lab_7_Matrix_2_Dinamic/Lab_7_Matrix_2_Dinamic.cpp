// Lab_7_Matrix_2_Dinamic.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>

class mxd
{
protected:
	int rows;
	int columns;
	double *matr;
public:
	mxd();
	~mxd();
	virtual void create();
	virtual void input();
	void print();
	int getrows();
	int getcolumns();
	bool summ(mxd matr2);
	bool mult(mxd matr2);
	void transp();
	double getelem(int r, int c)
	{
		return matr[r*columns + c];
	}
	void del();
};


mxd::mxd()
{
}

mxd::~mxd()
{
}

void mxd::del()
{
	if (matr != NULL) {
		delete matr;
		matr = NULL;
	}
	else
	{
		std::cout << "ERROR matrix already null";
	}
}

void mxd::create()
{
	int rs;
	int cs;
	std::cout << "Rows = ";
	std::cin >> rs;
	std::cout << "Columns = ";
	std::cin >> cs;
	std::cout << "\n";
	rows = rs;
	columns = cs;
	matr = new double[rows*columns];
}

void mxd::input()
{
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < columns; j++)
		{
			std::cout << "matrix[" << i + 1 << "][" << j + 1 << "] = ";
			std::cin >> matr[i*columns + j];
		}
	std::cout << "\n";
}

void mxd::print()
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			std::cout << matr[i*columns + j] << "\t";
		}
		std::cout << std::endl;
	}
	std::cout << "\n";
}

int mxd::getrows()
{
	return rows;
}

int mxd::getcolumns()
{
	return columns;
}

bool mxd::summ(mxd matr2)
{
	if (rows < matr2.getrows())
		return false;
	if (columns < matr2.getcolumns())
		return false;

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matr[i*columns + j] += matr2.getelem(i, j);
		}

	}
	return true;
}

bool mxd::mult(mxd matr2)
{
	if (columns != matr2.getrows())
	{
		std::cout << "ERROR \n" << std::endl;
		return false;
	}

	double *matr3 = new double[rows*matr2.getcolumns()];

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matr3[i*columns + j] = matr[i*columns + j];
		}
	}
	int temp = columns;
	columns = matr2.getcolumns();
	for (int j = 0; j < matr2.getcolumns(); j++)
	{
		for (int i = 0; i < rows; i++)
		{
			double x = 0;
			for (int t = 0; t < temp; t++)
			{
				x += matr3[i*temp + t] * matr2.getelem(t, j);
			}
			matr[i*columns + j] = x;
		}
	}
	delete matr3;
	return true;
}

void mxd::transp()
{
	double * matr3 = new double[rows*columns];

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matr3[i*columns + j] = matr[i*columns + j];
		}
	}

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matr[j*rows + i] = matr3[i*columns + j];
		}
	}
	int temp = rows;
	rows = columns;
	columns = temp;
	delete matr3;
}



class vcd : public mxd
{
public:

	vcd();
	double scalmultvector(vcd vec2);
	void xmult(double k);
	virtual void create();
	virtual void input();
};

void vcd::create()
{
	int rs = 1;
	int cs;
	std::cout << "Rows = 1" << std::endl;
	std::cout << "Columns = ";
	std::cin >> cs;
	columns = cs;
	rows = rs;
	matr = new double[rows*columns];
}

void vcd::input()
{
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < columns; j++)
		{
			std::cout << "Vec[" << j + 1 << "] = ";
			std::cin >> matr[i*columns + j];
		}
	std::cout << "\n";
}

vcd::vcd()
{
}

double vcd::scalmultvector(vcd vec2)
{
	vcd tmp = *this;
	if (tmp.mult(vec2))
	{
		return tmp.getelem(0, 0);
	}
	tmp = *this;
	tmp.transp();
	if (tmp.mult(vec2))
	{
		return tmp.getelem(0, 0);
	}
	return -1;
}

void vcd::xmult(double k)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matr[i*columns + j] *= k;
		}
	}
}


int main()
{
	std::cout << "lab7\n" << std::endl;
	mxd A;
	mxd B;
	std::cout << "Input A  \n" << std::endl;
	A.create();
	A.input();
	std::cout << "A = \n" << std::endl;
	A.print();
	std::cout << "Input B  \n" << std::endl;
	B.create();
	B.input();
	std::cout << "B = \n" << std::endl;
	B.print();
	A.summ(B);
	std::cout << "A + B = \n" << std::endl;
	A.print();
	A.transp();
	std::cout << "(A + B) TRANSPOSED = \n" << std::endl;
	A.print();
	A.mult(B);
	std::cout << "((A + B) TRANSPOSED) * B = \n" << std::endl;
	A.print();

	vcd C;
	vcd D;
	std::cout << "Input vec(C)  \n" << std::endl;
	C.create();
	C.input();
	std::cout << "vec(C) = \n" << std::endl;
	C.print();
	std::cout << "Input vec(D)  \n" << std::endl;
	D.create();
	D.input();
	std::cout << "vec(D) = \n" << std::endl;
	D.print();
	C.summ(D);
	std::cout << "vec(C) + vec(D) = \n" << std::endl;
	C.print();
	C.transp();
	std::cout << "(vec(C) + vec(D)) TRANSPOSED = \n" << std::endl;
	C.print();
	C.xmult(7);
	std::cout << "7*(vec(C) + vec(D)) TRANSPOSED = \n" << std::endl;
	C.print();
	double a = C.scalmultvector(D);
	std::cout << "(7*(vec(C) + vec(D)) TRANSPOSED) * vec(D) = " << a << std::endl;


	A.del();
	B.del();
	C.del();
	D.del();

	system("pause");
	return 0;
}
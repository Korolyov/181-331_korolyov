// Lab_6_Matrix.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>

class matrix
{
protected:
	double matr[10][10];
		int rows;
	int	columns;
public:
	matrix();
	~matrix();
	bool plus(matrix matr2);
	bool transp();
	bool mult(matrix matr2);
	virtual bool input();
	void print();

	int getrows()
	{
		return rows;
	}

	int getcolumns()
	{
		return columns;
	}

	double getelem(int r, int c)
	{
		if (r < rows && c<columns && c>-1)
			return matr[r][c];
		std::cout << "CANNOT GET ELEMENT. ERROR\n";
		return -1;
	}
};

matrix::matrix()
{

}
matrix::~matrix()
{

}
bool matrix::plus(matrix matr2)
{
	if (rows != matr2.getrows())
		return false;
	if (columns != matr2.getcolumns())
		return false;

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
			matr[i][j] += matr2.getelem(i, j);
	}
	return true;
}
bool matrix::input()
{
	int rs;
	int cs;
	std::cout << "Rows = ";
	std::cin >> rs;
	if (rs < 0)
	{
		std::cout << "ERROR ROWS  \n" << std::endl;
		return false;
	}
	if (rs > 10)
	{
		std::cout << "ERROR ROWS  \n" << std::endl;
		return false;
	}
	std::cout << "Columns = ";
	std::cin >> cs;
	std::cout << "\n";
	if (cs < 0)
	{
		std::cout << "ERROR COLUMNS  \n" << std::endl;
		return false;
	}
	if (cs > 10)
	{
		std::cout << "ERROR COLUMNS  \n" << std::endl;
		return false;
	}

	for (int i = 0; i < rs; i++)
		for (int j = 0; j < cs; j++)
			std::cin >> matr[i][j];
	columns = cs;
	rows = rs;
	std::cout << "\n";
	return true;
}

void matrix::print()
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
			std::cout << matr[i][j] << "\t";
		std::cout << std::endl;
	}
	std::cout << "\n";
}

bool matrix::transp()
{
	double matr3[10][10];

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matr3[i][j] = matr[i][j];
		}
	}

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matr[j][i] = matr3[i][j];
		}
	}
	int temp = rows;
	rows = columns;
	columns = temp;

	return true;
}

bool matrix::mult(matrix matr2)
{

	if (columns != matr2.getrows())
	{
		std::cout << "ERROR \n" << std::endl;
		return false;
	}

	double matr3[10][10];

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matr3[i][j] = matr[i][j];
		}
	}

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < matr2.getcolumns(); j++)
		{
			double x = 0.0;
			for (int t = 0; t < columns; t++)
			{
				x += matr3[i][t] * matr2.getelem(t, j);
			}
			matr[i][j] = x;

		}
	}
	columns = matr2.getcolumns();
	return true;
}


class vector : public matrix
{
public:

	vector();
	double scalmultvector(vector vec2);
	void xmult(double k);
	virtual bool input();
};

bool vector::input()
{
	int rs = 1;
	int cs;
	std::cout << "Rows = 1" << std::endl;
	if (rs < 0)
		return false;
	if (rs > 10)
		return false;
	std::cout << "Columns = ";
	std::cin >> cs;
	for (int i = 0; i < rs; i++)
		for (int j = 0; j < cs; j++)
			std::cin >> matr[i][j];
	columns = cs;
	rows = rs;
	std::cout << "\n";
	return true;
}


vector::vector()
{
}

double vector::scalmultvector(vector vec2)
{
	vector tmp = *this;
	if (tmp.mult(vec2))
	{
		return tmp.getelem(0, 0);
	}
	tmp = *this;
	tmp.transp();
	if (tmp.mult(vec2))
	{
		return tmp.getelem(0, 0);
	}
	return -1;
}

void vector::xmult(double k)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matr[i][j] *= k;
		}
	}
}

int main()
{
	std::cout << "lab6\n" << std::endl;
	std::cout << "DO NOT USE NUMBERS MORE THAN 10 FOR ROWS AND COLUMNS\n" << std::endl;
	matrix matrA;
	std::cout << "INPUT A\n" << std::endl;
	matrA.input();
	std::cout << "A = \n";
	matrA.print();
	matrix matrB;
	std::cout << "INPUT B\n" << std::endl;
	matrB.input();
	std::cout << "B = \n";
	matrB.print();
	matrA.plus(matrB);
	std::cout << "A + B = \n";
	matrA.print();
	matrA.transp();
	std::cout << "A transposed = \n";
	matrA.print();
	std::cout << "A * B = \n";
	matrA.mult(matrB);
	matrA.print();
	vector matrA1;
	vector matrB1;
	std::cout << "INPUT VECTOR A\n" << std::endl;
	matrA1.input();
	std::cout << "VECTOR A = \n";
	matrA1.print();
	std::cout << "INPUT VECTOR B\n" << std::endl;
	matrB1.input();
	std::cout << "VECTOR B = \n";
	matrB1.print();
	matrA1.transp();
	std::cout << "VECTOR A TRANSPOSED = \n";
	matrA1.print();
	matrA1.xmult(5);
	std::cout << "VECTOR A * 5 = \n";
	matrA1.print();
	matrA1.plus(matrB1);
	matrA1.print();
	double f = matrA1.scalmultvector(matrB1);
	std::cout << "ScalMult =\t" << f << std::endl;
	getchar();
	return 0;
}

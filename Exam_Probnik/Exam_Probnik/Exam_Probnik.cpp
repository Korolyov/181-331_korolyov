// Exam_Probnik.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <fstream>

class myArray
{
private:
	int n; //длина массива
	char *mass; //указатель на массив

public:
	//конструктор без параметров
	myArray()
	{
		n = 0;
		mass = new char[n];
	}

	myArray(char array[], int length)
	{
		n = length;
		mass = new char[n];
		for (int i = 0; i < n; i++)
		{
			mass[i] = array[i];
		}
	}

	myArray(const myArray & m)
	{
		n = m.n;
		mass = new char[n];
		for (int i = 0; i < n; i++)
		{
			mass[i] = m.mass[i];
		}
	}

	myArray(std::string s)
	{
		n = s.length();
		mass = new char[n];
		for (int i = 0; i < n; i++)
		{
			mass[i] = s[i];
		}
	}

	~myArray()
	{
		if (mass)
		{
			delete[] mass;
		}
	}
	char* getMass()
	{
		return mass;
	}
	int getN()
	{
		return n;
	}

	void show()
	{
		std::cout << "Number of simbols = " << n << std::endl;
		for (int i = 0; i < n; i++)
		{
			std::cout << mass[i];
		}
		std::cout << "\n";
	}

	void clear()
	{
		n = 0;
		delete[] mass;
		mass = new char[n];
	}

	//метод add() добавляет символы в конец массива
	void add(std::string s)
	{
		std::string myString(mass, n);
		delete[] mass;
		n = n + s.length();
		mass = new char[n];
		for (int i = 0; i < n; i++)
		{
			if (i < myString.length())
			{
				mass[i] = myString[i];
			}
			else
			{
				mass[i] = s[i - myString.length()];
			}
		}
	}
	void add(char array[], int length)
	{
		std::string myString(mass, n);
		delete[] mass;
		n = n + length;
		mass = new char[n];
		for (int i = 0; i < n; i++)
		{
			if (i < myString.length())
			{
				mass[i] = myString[i];
			}
			else
			{
				mass[i] = array[i - myString.length()];
			}
		}

	}

	//вставляет на заданную позицию внутри строки указанную в параметрах последовательность символов
	void insert(std::string s, int pozition)
	{
		std::string myString(mass, n);
		myString.insert(pozition - 1, s);
		delete[] mass;
		n = n + s.length();
		mass = new char[n];
		for (int i = 0; i < n; i++)
		{
			mass[i] = myString[i];
		}
	}
	void insert(char array[], int length, int pozition)
	{
		std::string myString(mass, n);
		std::string s(array, length);
		myString.insert(pozition - 1, s);
		delete[] mass;
		n = n + length;
		mass = new char[n];
		for (int i = 0; i < n; i++)
		{
			mass[i] = myString[i];
		}
	}

	void cut(int pozition, int count)
	{
		std::string myString(mass, n);
		delete[] mass;
		myString.erase(pozition - 1, count);
		n = n - count;
		mass = new char[n];
		for (int i = 0; i < n; i++)
		{
			mass[i] = myString[i];
		}
	}

	void find(std::string s)
	{
		std::string myString(mass, n);
		int p = myString.find(s);
		if (p == -1)
		{
			std::cout << "Not found!" << std::endl;
		}
		else
		{
			std::cout << "This string start with simbol number " << p + 1 << std::endl;
		}
	}
	void find(char array[], int length)
	{
		std::string myString(mass, n);
		std::string s(array, length);
		int p = myString.find(s);
		if (p == -1)
		{
			std::cout << "Not found!" << std::endl;
		}
		else
		{
			std::cout << "This string start with simbol number " << p + 1 << std::endl;
		}
	}

	void save(std::string fileName)
	{
		std::string myString(mass, n);
		std::ofstream fout(fileName);
		fout << myString << std::endl;
		fout.close();
	}

	void load(std::string fileName)
	{
		std::ifstream fin(fileName);
		if (!fin)
		{
			std::cout << "File not open!" << std::endl;
		}
		else
		{
			std::string s;
			std::getline(fin, s);
			n = s.length();
			delete[] mass;
			mass = new char[n];
			for (int i = 0; i < n; i++)
			{
				mass[i] = s[i];
			}
		}
	}
};


//перегрузка оператора плюс 
myArray operator + (myArray &obj1, myArray &obj2)
{
	int n = obj1.getN() + obj2.getN();
	char *m = new char[n];
	for (int i = 0; i < n; i++)
	{
		if (i < obj1.getN())
		{
			m[i] = obj1.getMass()[i];
		}
		else
		{
			m[i] = obj2.getMass()[i - obj1.getN()];
		}
	}
	myArray obj(m, n);
	return obj;
}

std::ostream& operator << (std::ostream &s, myArray obj)
{
	std::string str(obj.getMass(), obj.getN());
	s << "Count of simbols = " << obj.getN() << std::endl << "Array (string): " << str << std::endl;
	return s;
}

int main() {

	char array[] = { 'a', 'a', 'a', 'a' };
	int count = sizeof(array) / sizeof(char);
	myArray myObject1(array, count);
	myObject1.show();

	myArray m(myObject1);
	m.show();

	std::string str = "aaaaaaaa";
	myArray myObject2(str);
	myObject2.show();

	std::cout << "aaaaaaa" << std::endl;
	myObject1.clear();
	myObject1.show();

	std::cout << "aaaaaaaaaa" << std::endl;
	myArray myObject3("aaaaaaaa");
	myObject3.add("aaaaaaa");
	myObject3.show();
	myObject3.add(array, count);
	myObject3.show();

	std::cout << "aaaa" << std::endl;
	myArray myObject4("aaaa");
	myObject4.insert("aaaa", 4);
	myObject4.show();
	myObject4.insert(array, count, 2);
	myObject4.show();

	std::cout << "aaaa" << std::endl;
	myArray myObject5("aaaaaaaaaaaa");
	myObject5.cut(4, 11);
	myObject5.show();

	std::cout << "Cry" << std::endl;
	myArray myObject6 = myArray();
	for (int i = 0; i < 10000; i++) {
		myObject6.add("aaaaaaaaa");
		myObject6.insert("aaaaaaaaa", 4);
		myObject6.clear();
	}

	std::cout << "aaaaaaa" << std::endl;
	myArray myObject7("aaaaaa");
	myArray myObject8("aaaaaa");
	(myObject7 + myObject8).show();

	std::cout << "aaaaaaaa" << std::endl;
	std::cout << myObject8;

	std::cout << "aaaaaaa" << std::endl;
	myArray myObject9("aaaaaaaa");
	myObject9.find("228");
	myObject9.find("ggwp");
	myArray myObject10("aaaaaaaaaaaaaa");
	myObject10.find(array, count);

	std::cout << "aaaaaaaaa" << std::endl;
	myObject5.save("aaaaaaaa");

	std::cout << "aaaaaaaaa" << std::endl;
	myObject10.load("aaaaaaa");
	std::cout << myObject10;

	return 0;
}
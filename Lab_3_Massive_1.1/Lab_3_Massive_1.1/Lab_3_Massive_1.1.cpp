// Lab_3_Massive_1.1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>


int main()
{
	int Int = sizeof(int);
	int Bool = sizeof(bool);
	int Char = sizeof(char);
	int Long = sizeof(long);
	int Double = sizeof(double);

	std::cout << "int - " << Int << "\n";
	std::cout << "bool - " << Bool << "\n";
	std::cout << "char - " << Char << "\n";
	std::cout << "long - " << Long << "\n";
	std::cout << "double - " << Double << "\n";
}